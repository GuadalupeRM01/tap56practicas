package minicad;


import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LaboratorioU005_11
 */
public class Texto extends Figura {
   Point punto1;
   String texto;

   Texto(String _texto,Point _punto1, Color _color) {
        punto1 = new Point(_punto1.x,_punto1.y);        
        texto = _texto;
        color  = _color;
   }

   void dibujar(Graphics2D g2d){
        if (this.color!=null){
            g2d.setColor(color);
        }                
        g2d.drawString(texto,punto1.x,punto1.y);
   }
}

